<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('produtos', 'ProdutosController');
Route::resource('mesas', 'MesasController');
Route::resource('categorias', 'CategoriasController');
Route::resource('contas', 'ContasController');
Route::resource('conta_produto', 'ContaProdutoController');
Route::resource('itens', 'ItensController');

Route::post('conta_produto/inserirVarios', 'ContaProdutoController@storeMany');
Route::get('/produtos/{id}/buscarItens', 'ProdutosController@buscarItens');
Route::get('/produtos/buscarMaisVendidos/{quantidade}', 'ProdutosController@maisVendidos');
Route::get('/mesas/buscarPedidos/{id}/{idFuncionario}', 'MesasController@buscarPedidos');
Route::get('/categorias/buscarProdutos/{id}', 'CategoriasController@buscarProdutos');

Route::get('/inserirProduto', function() {
	return view('inserirProduto');
});

Route::post('/produtos/{id}', ['as' => 'inserirProduto', 'uses' => function (Request $request, $id) {
	    // Get the file from the request
	    $file = $request->file('image');

	    // Get the contents of the file
	    $contents = $file->openFile()->fread($file->getSize());

	    // Store the contents to the database
	    $produto = new App\Produto();
	    $produto->nome = "teste";
	    $produto->precoCompra = 10;
	    $produto->precoVenda = 10;
	    $produto->urlImagem = "/teste";
	    $produto->ativo = 1;
	    $produto->imagem = $contents;
	    $produto->save();
	    return redirect()->back();
	}
]);


