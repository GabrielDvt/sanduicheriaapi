<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;


class Mesa extends Model {

	protected $table = 'mesas';

	public function contaAberta()
	{
		return $this->hasMany('App\Conta')->where('encerrada', '=', 0);
	}

}
