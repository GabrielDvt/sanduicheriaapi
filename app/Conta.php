<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Produto;

class Conta extends Model {

	protected $table = 'contas';
	protected $attributes = array(
			'valor' => 0.0,
			'encerrada' => 0
		);

	public function mesa()
	{
		return $this->belongsTo('App\Mesa');
	}

	public function produtos()
	{
		return $this->belongsToMany('App\Produto')->withPivot('created_at', 'id');
	}
}
