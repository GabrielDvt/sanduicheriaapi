<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContaProduto extends Model
{
    protected $table = 'conta_produto';
    protected $attributes = array(
		'precoFinal' => 0.0
	);
}
