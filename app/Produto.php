<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;
use App\Item;

class Produto extends Model {

	protected $table = 'produtos';	

	public function itens()
	{
		return $this->belongsToMany('App\Item');
	}

	public function categoria()
	{
		return $this->belongsTo('App\Categoria');
	}

	public function contas()
	{
		return $this->belongsToMany('App\Conta');
	}
}
