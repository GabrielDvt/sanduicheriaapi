<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Item;
use App\ContaProduto;
use DB;

class ProdutosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::all();
        return response()->json($produtos);
    }

    /*  */
    public function maisVendidos($quantidade)
    {
        $ids = ContaProduto::select('produto_id', DB::raw('count(*) as total'))
            ->groupBy('produto_id')
            ->orderBy('total', 'DESC')
            ->limit($quantidade)->pluck('produto_id')->toArray();
        
        $orders = array_map(function($item) {
            return "id = {$item} desc";
        }, $ids);

        $rawOrder = implode(', ', $orders);
        
        $produtos = Produto::whereIn('id', $ids)->orderByRaw($rawOrder)->get();

        return response()->json($produtos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    retorna todos os itens de um determinado produto e os itens restantes para preencher a tabela de itens
    */
    public function buscarItens($id)
    {
        $produto = Produto::find($id);
        
        //busca todos os itens cadastrados
        $todosItens = Item::all();
        
        //busca os itens do produto
        $itensDoProduto = $produto->itens;

        //retorna os produtos que NÃO PERTENCEM ao item + os produtos que PERTENCEM ao item
        $collection1 = collect($todosItens);
        $diff = $collection1->diffKeys($itensDoProduto)->values()->all();
       
        $return = ['itensDoProduto' => $itensDoProduto, 'diferenca' => $diff];

        return response()->json($return);
    }
}


