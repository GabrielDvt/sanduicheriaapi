<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mesa;
use App\Conta;

class MesasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesas = Mesa::all();
         foreach($mesas as $mesa) {
            if ($mesa->contaAberta()->count() > 0) {
                $mesa->aberta = 1;
            } else {
                $mesa->aberta = 0;
            }
         }
        return response()->json($mesas);
    }

    /*
        Retorna um objeto com pedidos e com a conta relacionada
    */
    public function buscarPedidos($id, $idFuncionario) 
    {
        //verifica se uma conta existe para uma determinada mesa.
        $mesa = Mesa::find($id);   

        if ($mesa->contaAberta->count() == 0) {
            $conta = new Conta();
            $conta->mesa_id = $id;
            $conta->funcionario_id = $idFuncionario;
            $conta->save();
     
            return response()->json(['pedidos' => [], 'conta' => $conta]);

        } else {
            //dado o id da mesa, buscar a conta associada
            $conta = $mesa->contaAberta()->first();

            //busca os pedidos desta conta por ordem decrescente de horário (do mais recente para o mais antigo)
            $pedidos = $conta->produtos()->orderBy('pivot_created_at', 'desc')->get();

            return response()->json(['pedidos' => $pedidos, 'conta' => $conta]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
