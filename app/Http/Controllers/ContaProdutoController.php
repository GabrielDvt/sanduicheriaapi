<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContaProduto;

class ContaProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $contaProduto = new ContaProduto();
        $contaProduto->conta_id = $request->input('conta_id');
        $contaProduto->produto_id = $request->input('produto_id');
        $contaProduto->save();

        return response()->json(['mensagem' => 'Produto inserido com sucesso!']);
    }

    //insere vários produtos ao mesmo tempo
    public function storeMany(Request $request) 
    {
        $idConta = $request->input('conta_id');
        $produtos = $request->input('produtos');

        foreach($produtos as $produto) {
            //adiciona o produto N vezes (quantidade)
            for ($i = 0 ; $i < $produto['quantidade']; $i++) {
                $cp = new ContaProduto();
                $cp->conta_id = $idConta;
                $cp->produto_id = $produto['id'];
                $cp->precoFinal = $produto['precoVenda'];
                $cp->save();
            }
            
        }

        return response()->json($produtos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cp = ContaProduto::find($id);
        $cp->delete();

        return response()->json(['mensagem', 'Excluído com sucesso!']);
    }
}
