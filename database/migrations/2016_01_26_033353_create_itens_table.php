<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 150);
			$table->float('precoCompra');
			$table->float('precoVenda');
			$table->string('urlImagem', 250);
			$table->integer('ativo')->default("1");

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens');
	}

}
