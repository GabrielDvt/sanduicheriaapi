<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemProduto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_produto', function(Blueprint $table) {
			$table->integer('produto_id')->unsigned();
			$table->integer('item_id')->unsigned();

			$table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
			$table->foreign('item_id')->references('id')->on('itens')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_produto');
	}

}
