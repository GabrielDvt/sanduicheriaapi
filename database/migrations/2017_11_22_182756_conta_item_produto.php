<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContaItemProduto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conta_item_produto', function(Blueprint $table) {
            $table->integer('produto_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->integer('conta_id')->unsigned();

            $table->foreign('conta_id')->references('id')->on('contas')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('itens')->onDelete('cascade');
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conta_item_produto');
    }
}
